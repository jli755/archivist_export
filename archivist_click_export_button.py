#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to click the "Export" button
        - from https://archivist.closer.ac.uk/admin/export 
"""

import pandas as pd
import time
import sys
import os

from mylib import get_driver, url_base, archivist_login_all, get_names


driver = get_driver()


def click_export_button(df, uname, pw):
    """
    Loop over xml_name dictionary, click 'Export'
    """

    export_name = get_names(df)
    print("Got {} xml names".format(len(export_name)))
    print(df)
    print(export_name)

    ok = archivist_login_all(driver, export_name.values(), uname, pw)

    k = 0
    for prefix, url in export_name.items():
        if url:
            base = url_base(url)
            print('Working on item "{}" from "{}" with URL "{}"'.format(prefix, base, url))
            if not ok[base]:
                print("host {} was not available, skipping".format(base))
                continue
            driver.get(url)
            time.sleep(10)

            print(k)
            # find the input box
            inputElement = driver.find_element_by_xpath('//input[@placeholder="Search for..."]')

            inputElement.send_keys(prefix)

            # locate id and link
            trs = driver.find_elements_by_xpath("html/body/div/div/div/div/div/div/table/tbody/tr")

            for i in range(1, len(trs)):
                # row 0 is header: tr has "th" instead of "td"
                tr = trs[i]

                # column 2 is "Prefix"
                xml_prefix = tr.find_elements_by_xpath("td")[1].text

                # column 6 is "Actions", click on "export"
                exportButton = tr.find_elements_by_xpath("td")[5].find_elements_by_xpath("a")[1]

                if (xml_prefix == prefix):
                    print("Click export button for " + prefix)
                    exportButton.click()
                    time.sleep(5)
                else:
                    print("Did not find " + prefix)
    driver.quit()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]
    # prefixes
    df = pd.read_csv('Prefixes_to_export.txt', sep='\t')

    click_export_button(df, uname, pw)


if __name__ == "__main__":
    main()

